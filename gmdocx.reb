Rebol [
  date: 7-April-2019
  notes: {
    Docx templating test using JS and Rebol
  
    Ask a few questions, then generate a JS function which we push to the DOM.
    This should convert the template docx to be filled with our data which you download
  }
]

for-each site [
  https://cdnjs.cloudflare.com/ajax/libs/docxtemplater/3.9.1/docxtemplater.js
  https://cdnjs.cloudflare.com/ajax/libs/jszip/2.6.1/jszip.js
  https://cdnjs.cloudflare.com/ajax/libs/FileSaver.js/1.3.8/FileSaver.js
  https://cdnjs.cloudflare.com/ajax/libs/jszip-utils/0.0.2/jszip-utils.js
][
  js-do site
]

js-do {window.loadFile = function(url,callback){
        JSZipUtils.getBinaryContent(url,callback);
    };
}

definput: func [ description def <local> temp][
    temp: unspaced [description " (" def "): "]
    temp: ask temp
    if empty? temp [temp: def]
    return temp
]

GST: 1.15
mileage_rate: 0.79
GST_total: 0

;; get the week ending.  Calculate a default for this week
week_ending: 
if now/weekday = 1 [
    now
] else [
    now + 8 - now/weekday
]
week_ending: week_ending.date

cycle [
    endofweek: ask reduce [ spaced ["End of week" week_ending] 'text!]
    if empty? endofweek [break]
    attempt [
        endofweek: to date! endofweek
        week_ending: endofweek
        break
    ]
]

;; get invoice number.  Use default of reverse date
invoice_number: unspaced [week_ending.year next unspaced [100 + week_ending.month] next unspaced [100 + week_ending.day]]

invoice_number: definput "Invoice Number:" invoice_number

;; get hospital name
hospital: definput "Hospital:" "Palmerston North"

;; get days
comment {
cycle [
    days: definput "Days:" "5"
    attempt [days: to integer! days]
    if all [
        integer? days
        days > 0
        days < 8
    ][
        break
    ]
]
}

cycle [
    days: ask ["Days:" decimal!]
    if all [
        decimal? days
        days > 0
        days <= 7
    ][
        break
    ]
]
;; get rate
cycle [
    rate: definput "Daily Rate between 1-10,000:" "1500.0"
    attempt [rate: to decimal! rate]
    if all [
        decimal? rate
        rate > 0
        rate < 10000
    ][
        break
    ]
]

net_days: days * rate
GST_total: me + (net_days * GST - net_days)
comment {
;; taxis
taxis: 0
cycle [
    taxis: definput "Taxis cost including GST:" "0"
    attempt [taxis: to decimal! taxis]
    if all [
        decimal? taxis
        rate < 10000
    ][
        break
    ]
]
if not zero? taxis [
    taxis: taxis / GST
]
}

;; accom
accom: 0
cycle [
    accom: definput "accomodation cost including GST if any:" "0"
    attempt [accom: to decimal! accom]
    if all [
        decimal? accom
        rate < 10000
    ][
        break
    ]
]
if not zero? accom [
    ; accom: accom / GST
]


;; transport
cycle [
    transport: definput "Planes or long distance buses cost including GST:" "0"
    attempt [transport: to decimal! transport]
    if all [
        decimal? transport
        rate < 10000
    ][
        break
    ]
]
if not zero? transport [
    GST_total: me + (transport - (transport / GST))
    transport: transport / GST
]

;; driving car mileage

cycle [
    kms: definput "Kms driven:" "292"
    attempt [kms: to integer! kms]
    if all [
        integer? kms
        kms > -1
    ][
        break
    ]
]

comment {
cycle [
    kms: ask ["kms driven:" integer!]
    if all [
        kms >= 0
    ][
        break
    ]
]
}
kmcost: kms * mileage_rate ; this is ex GST
GST_total: me + (kmcost * GST) - kmcost

net_amount: net_days + accom + transport + kmcost
total: ((net_days + transport + kmcost) * GST) + accom
GST: GST_total

print spaced ["Kms:" kms]
print spaced ["Kmcost:" kmcost]
print spaced ["net amount: " net_amount]
print spaced ["total: " total]
print spaced ["GST: " GST]

data: {window.generate = function() {
        loadFile("https://s3.amazonaws.com/metaeducation/invoices/gm4.docx",function(error,content){
            if (error) { throw error };
            var zip = new JSZip(content);
            var doc=new window.docxtemplater().loadZip(zip)
            doc.setData({
                week_ending: '$a',
                week_ending2: '$a',
                invoice_number: '$b',
                hospital: '$c',
                days: '$d',
                rate: '$e',
                net_days: '$f',
                kms: '$l',
                kmcost: '$m',
                accom: '$g',
                transport: '$h',
                net_amount: '$i',
                GST: '$j',
                total: '$k'
            });
            try {
                // render the document (replace all occurences of {first_name} by John, {last_name} by Doe, ...)
                doc.render()
            }
            catch (error) {
                var e = {
                    message: error.message,
                    name: error.name,
                    stack: error.stack,
                    properties: error.properties,
                }
                console.log(JSON.stringify({error: e}));
                // The error thrown here contains additional information when logged with JSON.stringify (it contains a property object).
                throw error;
            }
            var out=doc.getZip().generate({
                type:"blob",
                mimeType: "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
            }) //Output the document using Data-URI
            saveAs(out,"$b-invoice.docx")
        })
    };
    generate()
}

template: reduce [
    'a  week_ending 
    'b invoice_number 
    'c hospital 
    'd days
    'e rate
    'f round/to to money! net_days $0.01
    'g round/to to money! accom $0.01
    'h round/to to money! transport $0.01
    'i round/to to money! net_amount $0.01
    'j round/to to money! GST $0.01
    'k round/to to money! total $0.01
    'l kms
    'm round/to to money! kmcost $0.01
]

; probe template

data: reword data template
; probe data

js-do data
print "Finished - docx should be downloaded."
