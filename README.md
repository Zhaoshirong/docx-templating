# docx-templating

Use rebol and JS to do docx templating

This uses JS libraries from https://docxtemplater.com/ to replace template variables inside a docx.

Rebol/browser is used to collect those variables from a console prompt.

Yes, this could alll be done in Rebol/browser but this just shows integration.

NB: If there is a cors error try

do https://cors-anywhere.herokuapp.com/https://gitlab.com/Zhaoshirong/docx-templating/-/raw/master/gmdocx.reb